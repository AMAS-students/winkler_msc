#set page(background: rect(width: 100%, height: 10%, fill: blue))
Undulator parameter K: unitless, page 20 \
$lambda_u$ is "period" \
$(2 pi) / lambda_u$ is $k_u$ \ 
$B$ is obtained through $(e B lambda) / (2 pi m c)$ \
Bunching factor: Eq 3.78 \
Shot noise: Longitudinal distribution: introduce sinusoidal perturbation: $delta x = chi gamma_0 k_u b_(f j) sin(2 chi gamma_0 k_u z + phi_j)$ where $phi_j$ describes random phase of bucket $j$ \
